# Not So Standard Substitution Cipher

I made a machine to implement a substitution cipher. The only issue is that it seemed to encrypt everything in sight, including my flag and all other random stuff. Each line in the attached file is a new piece of encoded information. Please save my flag!

The flag will not be encased in quack{...}, but it will be the only reasonable text.

*Author: Lachlan*